import { MigrationInterface, QueryRunner } from 'typeorm';

export class addDobToUser1641856000786 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "customFieldsDob" TIMESTAMP(6)`,
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "customFieldsDob"`,
      undefined
    );
  }
}
