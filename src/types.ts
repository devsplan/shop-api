import '@vendure/core';

declare module '@vendure/core' {
  export interface CustomUserFields {
    dob: string;
  }
}
